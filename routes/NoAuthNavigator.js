import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LoginScreen from "../screens/login-screen/LoginScreen";
import ForgotPassword from "../screens/forgot-password/ForgotPassword";
import SignUp from "../screens/sign-up/SignUp";
import Members from "../screens/members/Members";
import Groups from "../screens/groups/Groups";
import Cources from "../screens/cources/components/Cources";
import Profile from "../screens/profile/Profile";
import CoursesHome from "../screens/cources/CoursesHome";
import Home from "../screens/home/Home";
import More from "../screens/more/More";
import Settings from "../screens/settings/Settings";
const { Navigator, Screen } = createStackNavigator();

function NoAuthNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="LoginScreen" component={LoginScreen} />
      <Screen name="ForgotPassword" component={ForgotPassword} />
      <Screen name="SignUp" component={SignUp} />
      <Screen name="Members" component={Members} />
      <Screen name="Groups" component={Groups} />
      <Screen name="Cources" component={Cources} />
      <Screen name="Profile" component={Profile} />
      <Screen name="CoursesHome" component={CoursesHome} />
      <Screen name="Home" component={Home} />
      <Screen name="More" component={More} />
      <Screen name="Settings" component={Settings} />
    </Navigator>
  );
}
export const NoAuthNavigator = () => (
  <NavigationContainer>
    <NoAuthNavigation />
  </NavigationContainer>
);
