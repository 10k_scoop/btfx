import React from 'react';
import { StyleSheet, View } from 'react-native';
import { NoAuthNavigator } from './routes/NoAuthNavigator';

export default function App() {
  return (
    <View style={styles.container}>
      <NoAuthNavigator />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
