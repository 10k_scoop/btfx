import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { AntDesign } from "@expo/vector-icons";

const Search = ({screen}) => {
    return (
      <View style={{width:wp('100%'), backgroundColor:"#fff"}}>
        <View style={styles.conatiner}>
          <AntDesign name="search1" size={rf(18)} color="#A1A2A9" />
          <View style={styles.txtCont}>
            <TextInput style={styles.txt} placeholder="Search" />
          </View>
          {screen == "members" && (
            <AntDesign name="circledown" size={rf(16)} color="#A1A2A9" />
          )}
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  conatiner: {
    width: wp("94%"),
    height: hp("5%"),
    backgroundColor: "#F0F0F8",
    alignSelf: "center",
    borderRadius: 7,
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "3%",
    marginTop: hp('1%'),
    marginBottom: hp("2%"),
  },
  txtCont: {
    width: "89%",
    height: "100%",
    justifyContent:"center",
    paddingLeft:"3%"
  },
  txt: {
    width:'100%',
    height:"100%",
    color:"#000"
  }
});

export default Search;
