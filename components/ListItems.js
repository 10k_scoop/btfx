import { Entypo } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const ListItems = ({txt, badge}) => {
    return (
      <View style={styles.container}>
        <Text style={styles.txt}>{txt}</Text>
        {typeof badge == "number" ? (
          <View style={styles.badge}>
            <Text style={styles.badgeTxt}>{badge}</Text>
          </View>
        ) : (
          <View style={{height:"60%", justifyContent:"flex-end"}}>
            <Entypo name="chevron-down" size={rf(14)} color="#A1A2A9" />
          </View>
        )}
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    height: hp("4%"),
    borderWidth: 1,
    borderColor: "#EBEBEB",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderRadius: 30,
    flexDirection: "row",
    marginRight: wp("2%"),
    paddingHorizontal: wp("2%")
  },
  txt: {
    color: "#000",
    fontWeight: "bold",
    paddingHorizontal: wp("3%"),
    fontSize: rf(12)
  },
  badge: {
    height: hp("3%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F0F0F8",
    borderRadius: 20
  },
  badgeTxt: {
    fontWeight: "bold",
    fontSize: rf(11.5),
    color: "#f27e24",
    paddingHorizontal: wp("1.5%")
  }
});

export default ListItems;
