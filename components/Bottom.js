import { FontAwesome5, Ionicons, MaterialCommunityIcons, Octicons } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen';

const Bottom = ({active, onGroupsPress, onCourcesPress, onMembersPress, onHomePress, onMorePress}) => {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.iconCont} onPress={onHomePress}>
          <Octicons
            name="home"
            size={rf(20)}
            color={active == "home" ? "#63A6DE" : "#000"}
          />
          <Text
            style={[
              styles.txt,
              { color: active == "home" ? "#63A6DE" : "#000" }
            ]}
          >
            Home
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconCont} onPress={onCourcesPress}>
          <Ionicons
            name="book-outline"
            size={rf(20)}
            color={active == "cources" ? "#63A6DE" : "#000"}
          />
          <Text
            style={[
              styles.txt,
              { color: active == "cources" ? "#63A6DE" : "#000" }
            ]}
          >
            Cources
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconCont} onPress={onMembersPress}>
          <Ionicons
            name="people-outline"
            size={rf(20)}
            color={active == "members" ? "#63A6DE" : "#000"}
          />
          <Text
            style={[
              styles.txt,
              { color: active == "members" ? "#63A6DE" : "#000" }
            ]}
          >
            Members
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconCont} onPress={onGroupsPress}>
          <MaterialCommunityIcons
            name="account-group-outline"
            size={rf(20)}
            color={active == "groups" ? "#63A6DE" : "#000"}
          />
          <Text
            style={[
              styles.txt,
              { color: active == "groups" ? "#63A6DE" : "#000" }
            ]}
          >
            Groups
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconCont} onPress={onMorePress}>
          <Ionicons
            name="list-circle-outline"
            size={rf(20)}
            color={active == "more" ? "#63A6DE" : "#000"}
          />
          <Text
            style={[
              styles.txt,
              { color: active == "more" ? "#63A6DE" : "#000" }
            ]}
          >
            Morse
          </Text>
        </TouchableOpacity>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "flex-start",
    borderTopColor: "#EBEBEB",
    borderTopWidth: 1,
    paddingTop: hp('1.5%'),
  },
  iconCont: {
    alignItems: "center"
  },
  txt: {
    color: "#95959E",
    fontSize: rf(12)
  }
});

export default Bottom;
