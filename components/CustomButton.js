import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const CustomButton = ({text, screen, onPress}) => {
    return (
      <TouchableOpacity
        style={[
          styles.container,
          { width: screen == "login" ? wp("84%") : wp("90%") }
        ]}
        onPress={onPress}
      >
        <Text style={styles.txt}>{text}</Text>
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#5BADF1",
    height: hp('6%'),
    alignSelf:"center",
    borderRadius: 15,
    justifyContent:"center",
    alignItems:"center"
  },
  txt:{
      fontSize: rf(13),
      fontWeight:"bold",
      color:"#fff"
  }
});

export default CustomButton;
