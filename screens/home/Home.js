import React from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Bottom from '../../components/Bottom';
import CourceName from '../cources/components/CourceName';
import CourcesData from '../cources/components/CourcesData';
import Cources from './components/Cources';
import Groups from './components/Groups';
import Header from './components/Header';
import Members from './components/Members';

const Home = ({navigation}) => {
    const members = [1, 2, 3, 4, 5, 6];
  return (
    <View style={styles.container}>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>Home</Text>
      </View>
      <ScrollView>
        <Header name="Members" onPress={() => navigation.navigate("Members")} />
        <View>
          <ScrollView
            horizontal={true}
            style={styles.thumbnail}
            showsHorizontalScrollIndicator={false}
          >
            {members.map((i) => (
              <Members key={i} />
            ))}
          </ScrollView>
        </View>
        <Header
          name="Courses"
          onPress={() => navigation.navigate("CoursesHome")}
        />
        <View>
          <ScrollView
            horizontal={true}
            style={styles.thumbnail}
            showsHorizontalScrollIndicator={false}
          >
            {CourceName.map((i) => (
              <View key={i.id}>
                {CourcesData[i.name].items.slice(0, 1).map((item) => (
                  <Cources key={item.id} {...item} />
                ))}
              </View>
            ))}
          </ScrollView>
        </View>
        <Header name="Groups" onPress={() => navigation.navigate("Groups")} />
        <View>
          <ScrollView
            horizontal={true}
            style={styles.thumbnail}
            showsHorizontalScrollIndicator={false}
          >
            {members.map((i) => (
              <Groups key={i} />
            ))}
          </ScrollView>
        </View>
      </ScrollView>
      <Bottom
        active="home"
        onMembersPress={() => navigation.navigate("Members")}
        onGroupsPress={() => navigation.navigate("Groups")}
        onMorePress={() => navigation.navigate("More")}
        onCourcesPress={() => navigation.navigate("CoursesHome")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
    backgroundColor: "#fff"
  },
  txtCont: {
    paddingHorizontal: wp("4%"),
    marginTop: hp("3%"),
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
});

export default Home;
