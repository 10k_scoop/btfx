import React from "react";
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Groups = () => {
  return (
    <View
      style={{
        marginLeft: hp("1%"),
        height: hp("22%"),
        width: wp("43%")
      }}
    >
      <View style={styles.imgCont}>
        <ImageBackground
          source={require('../../../assets/plant.jpg')}
          resizeMode="cover"
          style={{
            width: "100%",
            height: "100%",
            justifyContent: "space-between"
          }}
        >
        </ImageBackground>
      </View>
      <View style={{ paddingHorizontal: wp("1%") }}>
        <Text numberOfLines={1} style={styles.name}>
          Business Meet
        </Text>
        <Text style={{ fontSize: rf(12), color: "#737170" }}>public/Group</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgCont: {
    height: hp("16%"),
    width: "100%",
    borderRadius: 10,
    overflow: "hidden"
  },
  cont: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    height: "25%",
    paddingHorizontal: "5%"
  },
  name: {
    fontSize: rf(16),
    fontWeight: "bold",
    paddingTop: wp("2%")
  }
});

export default Groups;
