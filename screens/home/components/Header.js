import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Header = ({name, onPress}) => {
    return (
      <View style={styles.txtCont}>
        <Text style={styles.heading2}>{name}</Text>
        <TouchableOpacity onPress={onPress} >
          <Text style={{ fontSize: rf(13), color: "#f27e24" }}>See all</Text>
        </TouchableOpacity>
      </View>
    );
}

const styles = StyleSheet.create({
  txtCont: {
    paddingHorizontal: wp("4%"),
    marginTop: hp("3%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    marginBottom: hp("2%")
  },
  heading2: {
    color: "#000",
    fontSize: rf(20),
    fontWeight: "bold"
  }
});

export default Header;
