import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Members = () => {
    return (
      <View style={styles.imgCont}>
        <Image
          source={require("../../../assets/display.jpg")}
          resizeMode="cover"
          style={{ width: "100%", height: "100%" }}
        />
      </View>
    );
}
const styles = StyleSheet.create({
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    overflow: "hidden",
    marginLeft: hp('1%')
  }
});

export default Members;
