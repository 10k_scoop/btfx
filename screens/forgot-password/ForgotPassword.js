import React from "react";
import {
  Dimensions,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import TextField from "../sign-up/components/TextField";
import { AntDesign } from "@expo/vector-icons";
import CustomButton from "../../components/CustomButton";

const ForgotPassword = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#0a4474", "#63A6DE", "#63A6DE"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <AntDesign name="left" size={rf(18)} color="#fff" />
      </TouchableOpacity>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>Forgot Password?</Text>
        <Text style={[styles.details,{marginTop:hp('2%')}]}>
          Enter Your Email Address and we'll send you instructions to reset you
          password
        </Text>
      </View>
      <TextField label={"Email Address"} />
      <View style={styles.buttCont}>
        <CustomButton text="Submit" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  back: {
    marginLeft: wp("4%"),
    alignSelf: "flex-start",
  },
  txtCont: {
    width: wp("84%"),
    alignSelf: "center",
    height: hp("25%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: hp("3%"),
  },
  heading: {
    color: "#fff",
    fontSize: rf(20),
    fontWeight: "bold",
  },
  details: {
    color: "#fff",
    fontSize: rf(12),
    textAlign: "center",
  },
  buttCont: {
    alignSelf: "center",
    alignContent: "flex-end",
    justifyContent: "flex-end",
    height: hp("52%"),
  },
});

export default ForgotPassword;
