import { AntDesign } from '@expo/vector-icons';
import React from 'react';
import { Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import List from './components/List';

const Profile = ({navigation}) => {
    return (
      <View style={styles.container}>
        <ScrollView>
          <View style={{ height: hp("50%") }}>
            <ImageBackground
              source={require("../../assets/cover.jpg")}
              resizeMode="cover"
              style={styles.cover}
            >
              <TouchableOpacity
                style={styles.back}
                onPress={() => navigation.goBack()}
              >
                <AntDesign name="left" size={rf(18)} color="#fff" />
              </TouchableOpacity>
            </ImageBackground>
            <View style={styles.cont2}>
              <View style={styles.cont3}>
                <View style={styles.imgCont}>
                  <Image
                    source={require("../../assets/display.jpg")}
                    resizeMode="cover"
                    style={{
                      width: hp("11%"),
                      height: hp("11%"),
                      borderRadius: 100
                    }}
                  />
                </View>
                <View style={styles.txtCont}>
                  <Text style={styles.name}>John</Text>
                  <Text style={styles.mail}>@John</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.list}>
            <List txt="Profile" />
            <List txt="Timeline" />
            <List txt="Connections" />
            <List txt="Groups" />
            <List txt="Courses" />
            <List txt="Photos" />
            <List txt="Forums" />
            <List txt="Email Invites" />
            <List txt="Points" />
            <List txt="Achievements" />
            <List txt="Ranks" />
          </View>
        </ScrollView>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  back: {
    marginLeft: wp("4%"),
    alignSelf: "flex-start",
    paddingTop: StatusBar.currentHeight + 10,
    zIndex: 999
  },
  cover: {
    width: wp("100%"),
    height: hp("30%")
  },
  cont2: {
    width: wp("88%"),
    height: hp("45%"),
    position: "absolute",
    alignSelf: "center",
    justifyContent: "flex-end",
    overflow: "hidden",
    borderRadius: 15,
    backgroundColor: "transparent"
  },
  imgCont: {
    width: hp("12%"),
    height: hp("12%"),
    borderRadius: 100,
    overflow: "hidden",
    backgroundColor: "#fff",
    zIndex: 999,
    alignSelf: "center",
    position: "absolute",
    alignItems: "center",
    justifyContent: "center"
  },
  cont3: {
    width: "100%",
    height: hp("25%"),
    backgroundColor: "transparent",
    alignSelf: "center",
    borderRadius: 15
  },
  txtCont: {
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
    marginTop: "12%",
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  name: {
    fontSize: rf(25),
    color: "#000",
    fontWeight: "bold"
  },
  mail: {
    fontSize: rf(14),
    color: "#000"
  },
  list:{
    width: wp('90%'),
    height: hp('83.5%'),
    backgroundColor:"#fff",
    borderRadius: 15,
    overflow:"hidden",
    alignSelf:"center",
    marginBottom: hp('5%')
  }
});

export default Profile;
