import {
  AntDesign,
  Entypo,
  Feather,
  FontAwesome,
  FontAwesome5,
  Ionicons,
  MaterialCommunityIcons
} from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const List = ({ txt }) => {
  return (
    <View>
      <View style={styles.container}>
        <View style={styles.icon}>
          <View style={{ width: wp("12%") }}>
            {txt == "Profile" && (
              <Ionicons name="person-outline" size={rf(24)} color="#000" />
            )}
            {txt == "Timeline" && (
              <Feather name="activity" size={rf(24)} color="#000" />
            )}
            {txt == "Connections" && (
              <Ionicons
                name="md-person-add-outline"
                size={rf(24)}
                color="#000"
              />
            )}
            {txt == "Groups" && (
              <Ionicons name="people-outline" size={rf(24)} color="#000" />
            )}
            {txt == "Courses" && (
              <AntDesign name="book" size={rf(24)} color="#000" />
            )}
            {txt == "Photos" && (
              <AntDesign name="picture" size={rf(24)} color="#000" />
            )}
            {txt == "Forums" && (
              <FontAwesome5 name="comments" size={rf(24)} color="#000" />
            )}
            {txt == "Email Invites" && (
              <MaterialCommunityIcons
                name="email-open-outline"
                size={rf(24)}
                color="#000"
              />
            )}
            {txt == "Points" && (
              <AntDesign name="staro" size={rf(24)} color="#000" />
            )}
            {txt == "Achievements" && (
              <FontAwesome5 name="award" size={rf(24)} color="#000" />
            )}
            {txt == "Ranks" && (
              <FontAwesome5 name="wine-glass" size={rf(24)} color="#000" />
            )}
          </View>
          <Text style={styles.txt}>{txt}</Text>
        </View>
        <Entypo name="chevron-right" size={rf(22)} color="#979797" />
      </View>
      <View style={styles.splitter}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("7.5%"),
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "5%"
  },
  icon: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "3%"
  },
  txt: {
    fontWeight: "bold",
    fontSize: rf(16)
  },
  splitter: {
    borderBottomWidth: 1,
    borderBottomColor: "#c6c8cc",
    width: "90%",
    alignSelf: "flex-end"
  }
});

export default List;
