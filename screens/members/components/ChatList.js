import { Entypo } from '@expo/vector-icons';
import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const ChatList = () => {
    return (
      <View>
        <View style={styles.cont1}>
          <View style={styles.imgCont}>
            <Image
              source={require("../../../assets/display.jpg")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
          <View style={styles.txtCont}>
            <Text style={styles.name}>Mark Franklin</Text>
            <Text style={styles.duration}>Admin</Text>
          </View>
          <Entypo name="dots-three-horizontal" size={rf(18)} color="#979797" />
        </View>
        <View style={styles.splitter}></View>
      </View>
    );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    height: hp("11%"),
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    alignItems: "center"
  },
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    overflow: "hidden"
  },
  txtCont: {
    width: "75%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "5%"
  },
  name: {
    fontSize: rf(16),
    color: "#000",
    fontWeight: "bold"
  },
  duration: {
    fontSize: rf(12),
    color: "#979797"
  },
  splitter: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    width: wp('70%'),
    alignSelf:"flex-end",
    marginRight: wp('5%')
  }
});

export default ChatList;
