import { MaterialIcons } from '@expo/vector-icons';
import React from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Bottom from '../../components/Bottom';
import ChatList from './components/ChatList';
import ListItems from '../../components/ListItems';
import Search from '../../components/Search';

const Members = ({navigation}) => {
  const items = [1,2,3,4,5,6,7,8,9,10]
    return (
      <View style={styles.container}>
        <View style={styles.txtCont}>
          <Text style={styles.heading}>Members</Text>
        </View>
        <Search screen="members" />
        <ScrollView>
          <View style={styles.line}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <View style={styles.icon}>
                <MaterialIcons
                  name="filter-list"
                  size={rf(18)}
                  color="#A1A2A9"
                />
              </View>
              <ListItems txt="All Members" badge={22} />
              <ListItems txt="Recently Active" />
              <ListItems txt="Recently Active" />
            </ScrollView>
          </View>
          <View style={styles.chat}>
            {items.map((item) => (
              <ChatList key={item} />
            ))}
          </View>
        </ScrollView>
        <Bottom
          active="members"
          onGroupsPress={() => navigation.navigate("Groups")}
          onCourcesPress={() => navigation.navigate("CoursesHome")}
          onMorePress={() => navigation.navigate("More")}
          onHomePress={() => navigation.navigate("Home")}
        />
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
    backgroundColor: "#fff"
  },
  txtCont: {
    marginLeft: wp("4%"),
    marginTop: hp("3%")
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
  line: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    borderTopColor: "#EBEBEB",
    borderTopWidth: 1,
    height: hp("7%"),
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: "5%",
    width: wp("100%")
  },
  icon: {
    width: hp("4%"),
    height: hp("4%"),
    borderWidth: 1,
    borderColor: "#EBEBEB",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginRight: wp("4%")
  },
  chat:{
    marginTop: hp('1%')
  }
});

export default Members;
