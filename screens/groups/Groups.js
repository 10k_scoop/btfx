import { AntDesign, MaterialIcons } from '@expo/vector-icons';
import React from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Bottom from '../../components/Bottom';
import ListItems from '../../components/ListItems';
import Search from '../../components/Search';
import GroupList from './components/GroupList'

const Groups = ({navigation}) => {

    return (
      <View style={styles.container}>
        <View style={styles.txtCont}>
          <Text style={styles.heading}>Groups</Text>
          <AntDesign name="pluscircle" size={rf(22)} color="#f27e24" />
        </View>
        <Search />
        <ScrollView>
          <View style={styles.line}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <View style={styles.icon}>
                <MaterialIcons
                  name="filter-list"
                  size={rf(18)}
                  color="#A1A2A9"
                />
              </View>
              <ListItems txt="All Groups" badge={311} />
              <ListItems txt="All Types" />
              <ListItems txt="Sort By" />
            </ScrollView>
          </View>
          <View style={styles.group}>
            <GroupList status="Join" />
            <GroupList status="Member" />
            <GroupList status="Request" />
            <GroupList status="Request" />
            <GroupList status="Request" />
          </View>
        </ScrollView>
        <Bottom
          active="groups"
          onMembersPress={() => navigation.navigate("Members")}
          onCourcesPress={() => navigation.navigate("CoursesHome")}
          onMorePress={() => navigation.navigate("More")}
          onHomePress={() => navigation.navigate("Home")}
        />
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
    backgroundColor: "#fff"
  },
  txtCont: {
    paddingHorizontal: wp("4%"),
    marginTop: hp("3%"),
    flexDirection:"row",
    justifyContent:"space-between",
    alignItems:"center"
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
  line: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    borderTopColor: "#EBEBEB",
    borderTopWidth: 1,
    height: hp("7%"),
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: "5%",
    width: wp("100%")
  },
  icon: {
    width: hp("4"),
    height: hp("4%"),
    borderWidth: 1,
    borderColor: "#EBEBEB",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginRight: wp("2%")
  },
  group:{
    marginTop: hp('1%')
  }
});

export default Groups;
