import { Entypo, Feather } from '@expo/vector-icons';
import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const GroupList = ({status}) => {
    return (
      <View>
        <View style={styles.cont1}>
          <View style={styles.imgCont}>
            <Image
              source={require("../../../assets/plant.jpg")}
              resizeMode="cover"
              style={{ width: "100%", height: "100%" }}
            />
          </View>
          <View style={styles.txtCont}>
            <View>
              <Text style={styles.name}>Architech Ideas</Text>
              <Text style={styles.duration}>public/school . 16 members</Text>
            </View>
            {status == "Member" ? (
              <View style={styles.tag2}>
                <Feather name="check" size={rf(14)} color="#A0A7AD" />
                <Text style={[styles.status, { color: "#A0A7AD" }]}>
                  {status}
                </Text>
              </View>
            ) : (
              <View
                style={[
                  styles.tag,
                  { width: status == "Join" ? wp("15%") : wp("22%") }
                ]}
              >
                <Text style={styles.status}>{status}</Text>
              </View>
            )}
          </View>
        </View>
        <View style={styles.splitter}></View>
      </View>
    );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    height: hp("15%"),
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    alignItems: "center"
  },
  imgCont: {
    height: hp("13%"),
    width: hp("13%"),
    borderRadius: 10,
    overflow: "hidden"
  },
  txtCont: {
    width: "70%",
    height: "100%",
    justifyContent: "space-between",
    paddingHorizontal: "5%",
    paddingVertical: "3%"
  },
  name: {
    fontSize: rf(16),
    color: "#000",
    fontWeight: "bold"
  },
  duration: {
    fontSize: rf(12),
    color: "#979797"
  },
  tag: {
    height: wp("7%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#D9DDF8",
    borderRadius: 20
  },
  tag2: {
    width: wp("22%"),
    height: wp("7%"),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EFF0F8",
    borderRadius: 20,
    flexDirection: "row"
  },
  status: {
    fontWeight: "bold",
    fontSize: rf(12),
    color: "#3869EB"
  },
  splitter: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    width: wp("70%"),
    alignSelf: "flex-end",
    marginRight: wp("5%")
  }
});

export default GroupList;
