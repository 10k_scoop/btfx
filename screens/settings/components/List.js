import {
  AntDesign,
  Entypo,
  Feather,
  Ionicons,
  MaterialIcons,
} from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const List = ({ txt, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.iconCont}>
          <View style={styles.icon}>
            {txt == "Login Infromation" && (
              <Ionicons name="person-outline" size={rf(24)} color="#000" />
            )}
            {txt == "Privacy Settings" && (
              <Feather name="lock" size={rf(24)} color="#000" />
            )}
            {txt == "Group Invites" && (
              <MaterialIcons name="group-add" size={rf(24)} color="#000" />
            )}
            {txt == "About" && (
              <Ionicons
                name="information-circle-outline"
                size={rf(24)}
                color="#000"
              />
            )}
            {txt == "Send us Feedback" && (
              <Feather name="thumbs-up" size={rf(24)} color="#000" />
            )}
            {txt == "Report a bug" && (
              <Ionicons name="bug-outline" size={rf(24)} color="#000" />
            )}
            {txt == "Export Data" && (
              <AntDesign name="clouddownloado" size={rf(24)} color="#000" />
            )}
            {txt == "Resent Sandbox" && (
              <Ionicons name="reload" size={rf(24)} color="#000" />
            )}
          </View>
          <Text style={styles.txt}>{txt}</Text>
        </View>
        {txt != "Export Data" && txt != "Resent Sandbox" && (
          <Entypo name="chevron-right" size={rf(22)} color="#979797" />
        )}
      </View>
      <View style={styles.splitter}></View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("7.5%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "5%"
  },
  iconCont: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "1%"
  },
  icon: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp("3%"),
    overflow: "hidden"
  },
  txt: {
    fontWeight: "bold",
    fontSize: rf(16)
  },
  splitter: {
    borderBottomWidth: 1,
    borderBottomColor: "#c6c8cc",
    width: "90%",
    alignSelf: "flex-end"
  }
});

export default List;
