import { AntDesign } from "@expo/vector-icons";
import React, { useState } from "react";
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  Switch,
  Text,
  View
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import List from "./components/List";

const Settings = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  return (
    <View style={styles.container}>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>Settings</Text>
      </View>
      <ScrollView>
        <View style={styles.cont}>
          <Text style={{ fontSize: rf(15), color: "#979797" }}>
            Logged in as
          </Text>
          <Text style={{ fontSize: rf(15), fontWeight: "bold" }}>John</Text>
        </View>
        <View style={[styles.list, { height: hp("45.5%") }]}>
          <List txt="Login Infromation" />
          <List txt="Privacy Settings" />
          <List txt="Group Invites" />
          <List txt="About" />
          <List txt="Send us Feedback" />
          <List txt="Report a bug" />
        </View>
        <View style={[styles.list, { height: hp("7.5%") }]}>
          <View style={styles.cont2}>
            <Text style={{ fontSize: rf(15), fontWeight: "bold" }}>
              Unlock with fingerprint
            </Text>
            <Switch
              trackColor={{ false: "#767577", true: "#29b765" }}
              thumbColor={isEnabled ? "#F1552C" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>
        <View style={[styles.list, { height: hp("7.5%") }]}>
          <List txt="Export Data" />
        </View>
        <View style={[styles.list, { height: hp("7.5%") }]}>
          <List txt="Resent Sandbox" />
        </View>
        <View
          style={[styles.list, { height: hp("7.5%"), marginBottom: hp("3%") }]}
        >
          <View style={[styles.cont2, { justifyContent: "center" }]}>
            <AntDesign
              name="logout"
              size={rf(20)}
              color="#F1552C"
              style={{ transform: [{ rotate: "270 deg" }] }}
            />
            <Text style={styles.txt}>Logout</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  txtCont: {
    paddingHorizontal: wp("4%"),
    backgroundColor: "#fff",
    paddingTop: StatusBar.currentHeight + hp("5%"),
    paddingBottom: hp("2%")
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
  cont: {
    backgroundColor: "#fff",
    justifyContent: "space-between",
    paddingHorizontal: "5%",
    alignItems: "center",
    flexDirection: "row",
    width: wp("90%"),
    alignSelf: "center",
    height: hp("7.5%"),
    borderRadius: 15,
    marginTop: hp("3%")
  },
  list: {
    backgroundColor: "#fff",
    alignSelf: "center",
    width: wp("90%"),
    borderRadius: 15,
    overflow: "hidden",
    marginTop: hp("3%")
  },
  cont2: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "5%",
    height: "100%",
    width: "100%"
  },
  txt: {
    fontSize: rf(15),
    fontWeight: "bold",
    color: "#F1552C",
    paddingLeft: "3%"
  }
});

export default Settings;
