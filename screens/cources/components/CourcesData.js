const CourcesData = {
  "Art & Music": {
    id: 1,
    title: "Art & Music",
    items: [
      {
        id: 1,
        name: "Animation and VFX",
        date: "October 23, 2020",
        status: "In progress",
        progress: "20%",
        author: "Emily",
        img: require("../../../assets/art/1.jpg")
      },
      {
        id: 2,
        name: "Become an awsome singer",
        date: "Feburary 17, 2020",
        status: "In progress",
        progress: "20%",
        author: "Sana",
        img: require("../../../assets/art/2.jpg")
      },
      {
        id: 3,
        name: "Character Art School: Complete Character Drawing",
        date: "October 16, 2020",
        status: "In progress",
        progress: "75%",
        author: "Sana",
        img: require("../../../assets/art/3.jpg")
      },
      {
        id: 4,
        name: "Intro to JS: Drawing & Animation",
        date: "October 13, 2020",
        status: "In progress",
        progress: "10%",
        author: "Emily",
        img: require("../../../assets/art/4.jpg")
      }
    ]
  },
  "Business And Finance": {
    id: 2,
    title: "Business And Finance",
    items: [
      {
        id: 1,
        name: "Contacting Small Business Owners",
        date: "October 09, 2020",
        status: "Start",
        progress: "0%",
        author: "Emily",
        img: require("../../../assets/business/1.jpg")
      },
      {
        id: 2,
        name: "Get Cash foy Your Annuity",
        date: "October 09, 2020",
        status: "Start",
        progress: "0%",
        author: "Emily",
        img: require("../../../assets/business/2.jpg")
      },
      {
        id: 3,
        name: "How to Fundraise",
        date: "October 08, 2020",
        status: "Start",
        progress: "0%",
        author: "John",
        img: require("../../../assets/business/3.jpg")
      },
      {
        id: 4,
        name: "MBA in a Box",
        date: "May 27, 2019",
        status: "Start",
        progress: "0%",
        author: "Sana",
        img: require("../../../assets/business/4.jpg")
      },
      {
        id: 5,
        name: "Project Management For Beginners",
        date: "February 17, 2020",
        status: "In progress",
        progress: "50%",
        author: "John",
        img: require("../../../assets/business/5.jpg")
      }
    ]
  },
  Designing: {
    id: 3,
    title: "Designing",
    items: [
      {
        id: 1,
        name: "Advanced JS: Games & Vicualization",
        date: "February 17, 2020",
        status: "In progress",
        progress: "50%",
        author: "John",
        img: require("../../../assets/designing/1.jpg")
      },
      {
        id: 2,
        name: "Animation and VFX",
        date: "October 23, 2020",
        status: "In progress",
        progress: "20%",
        author: "Emily",
        img: require("../../../assets/art/1.jpg")
      },
      {
        id: 3,
        name: "Character Art School: Complete Character Drawing",
        date: "October 16, 2020",
        status: "In progress",
        progress: "75%",
        author: "Sana",
        img: require("../../../assets/art/3.jpg")
      },
      {
        id: 4,
        name: "Inspiration 3D cource for Designers",
        date: "October 16, 2020",
        status: "In progress",
        progress: "90%",
        author: "Robert",
        img: require("../../../assets/designing/4.jpg")
      },
      {
        id: 5,
        name: "Intro to HTML/CSS: Making webpages",
        date: "October 13, 2020",
        status: "In progress",
        progress: "10%",
        author: "Emily",
        img: require("../../../assets/designing/3.jpg")
      },
      {
        id: 6,
        name: "Intro to JS: Drawing & Animation",
        date: "October 13, 2020",
        status: "In progress",
        progress: "10%",
        author: "Emily",
        img: require("../../../assets/art/4.jpg")
      }
    ]
  },
  "Programming Language": {
    id: 4,
    title: "Programming Language",
    items: [
      {
        id: 1,
        name: "Concept of Computer Engineering",
        date: "Feburary 17, 2020",
        status: "Completed",
        progress: "100%",
        author: "Robert",
        img: require("../../../assets/designing/2.jpg")
      },
      {
        id: 2,
        name: "Advanced JS: Games & Vicualization",
        date: "February 17, 2020",
        status: "In progress",
        progress: "50%",
        author: "John",
        img: require("../../../assets/designing/1.jpg")
      },
      {
        id: 3,
        name: "Intro to HTML/CSS: Making webpages",
        date: "October 13, 2020",
        status: "In progress",
        progress: "10%",
        author: "Emily",
        img: require("../../../assets/designing/3.jpg")
      },
      {
        id: 4,
        name: "Intro to JS: Drawing & Animation",
        date: "October 13, 2020",
        status: "In progress",
        progress: "10%",
        author: "Emily",
        img: require("../../../assets/art/4.jpg")
      }
    ]
  },
  "Home Economics": {
    id: 5,
    title: "Home Economics",
    items: [
      {
        id: 1,
        name: "Create an Eco-Friendly House",
        date: "October 07, 2020",
        status: "In progress",
        progress: "50%",
        author: "John",
        img: require("../../../assets/Home/1.jpg")
      },
      {
        id: 2,
        name: "Home Gardning Safety",
        date: "February 17, 2020",
        status: "Completed",
        progress: "100%",
        author: "Sana",
        img: require("../../../assets/Home/2.jpg")
      },
      {
        id: 3,
        name: "How to Prepare for a Drought",
        date: "October 09, 2020",
        status: "Start",
        progress: "0%",
        author: "Robert",
        img: require("../../../assets/Home/3.jpg")
      },
      {
        id: 4,
        name: "Pack Fruniture for Moving",
        date: "October 07, 2020",
        status: "Start",
        progress: "0%",
        author: "Sana",
        img: require("../../../assets/Home/4.jpg")
      }
    ]
  }
};

export default CourcesData
