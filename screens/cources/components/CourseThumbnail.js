import { AntDesign } from '@expo/vector-icons';
import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";


const CourseThumbnail = ({ status, progress, img, name, author }) => {
  return (
    <View
      style={{
        marginLeft: hp("1%"),
        height: hp("22%"),
        width: wp("43%")
      }}
    >
      <View style={styles.imgCont}>
        <ImageBackground
          source={img}
          resizeMode="cover"
          style={{
            width: "100%",
            height: "100%",
            justifyContent: "space-between"
          }}
        >
          <View
            style={[
              styles.statusCont,
              {
                width: status == "Start" ? "27%" : "47%",
                backgroundColor:
                  status == "Start"
                    ? "#f27e24"
                    : status == "Completed"
                    ? "#6fde2f"
                    : "#9C3DE7"
              }
            ]}
          >
            <Text style={styles.status}>{status}</Text>
          </View>
          <View style={styles.cont}>
            <View style={styles.StatusBar}>
              <View style={[styles.ProgressBar, { width: progress }]}></View>
            </View>
            <AntDesign name="clouddownloado" size={rf(24)} color="#fff" />
          </View>
        </ImageBackground>
      </View>
      <View style={{paddingHorizontal: wp('1%')}}>
        <Text numberOfLines={1} style={styles.name}>
          {name}
        </Text>
        <Text style={{ fontSize: rf(12), color: "#737170" }}>{author}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgCont: {
    height: hp("16%"),
    width: "100%",
    borderRadius: 10,
    overflow: "hidden"
  },
  statusCont: {
    marginTop: "10%",
    height: "15%",
    paddingLeft:2,
    justifyContent: "center",
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100
  },
  status: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: rf(12),
    paddingLeft: "4%"
  },
  StatusBar: {
    width: "75%",
    height: "15%",
    borderRadius: 5,
    backgroundColor: "#fff",
    overflow: "hidden",
    alignSelf: "center"
  },
  ProgressBar: {
    position: "absolute",
    height: "100%",
    backgroundColor: "#f27e24",
    borderRadius: 5
  },
  cont: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%",
    height: "25%",
    paddingHorizontal: "5%"
  },
  name:{
    fontSize: rf(16),
    fontWeight:"bold",
    paddingTop: wp('2%')
  }
});

export default CourseThumbnail;
