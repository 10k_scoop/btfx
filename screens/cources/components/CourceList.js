import { AntDesign } from '@expo/vector-icons';
import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const CourceList = ({ status, progress, name, author, date, img }) => {
  return (
    <View>
      <View style={styles.cont1}>
        <View style={styles.imgCont}>
          <ImageBackground
            source={img}
            resizeMode="cover"
            style={{
              width: "100%",
              height: "100%",
              justifyContent: "space-between"
            }}
          >
            <View
              style={[
                styles.statusCont,
                {
                  width: status == "Start" ? "40%" : "80%",
                  backgroundColor:
                    status == "Start"
                      ? "#f27e24"
                      : status == "Completed"
                      ? "#6fde2f"
                      : "#9C3DE7"
                }
              ]}
            >
              <Text style={styles.status}>{status}</Text>
            </View>
            <View style={styles.StatusBar}>
              <View style={[styles.ProgressBar, { width: progress }]}></View>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.txtCont}>
          <View>
            <Text style={styles.name}>
              {name}
            </Text>
            <Text style={(styles.name, { fontSize: rf(12), color: "#000" })}>
              {author}
            </Text>
          </View>
          <View style={styles.tag}>
            <Text style={styles.duration}>{date}</Text>
            <AntDesign name="clouddownloado" size={rf(24)} color="#f27e24" />
          </View>
        </View>
      </View>
      <View style={styles.splitter}></View>
    </View>
  );
};

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    height: hp("15%"),
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    alignItems: "center"
  },
  imgCont: {
    height: hp("13%"),
    width: hp("13%"),
    borderRadius: 10,
    overflow: "hidden",
  },
  txtCont: {
    width: "70%",
    height: "100%",
    justifyContent: "space-between",
    paddingHorizontal: "5%",
    paddingVertical: "3%"
  },
  name: {
    fontSize: rf(17),
    color: "#000",
    fontWeight: "bold"
  },
  duration: {
    fontSize: rf(12),
    color: "#979797"
  },
  tag: {
    height: wp("7%"),
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row"
  },
  statusCont: {
    marginTop: "10%",
    height: "15%",
    justifyContent: "center",
    borderTopRightRadius: 100,
    borderBottomRightRadius: 100
  },
  status: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: rf(12),
    paddingLeft: 2
  },
  StatusBar: {
    width: "85%",
    height: "4%",
    borderRadius: 5,
    backgroundColor: "#fff",
    overflow: "hidden",
    marginBottom: "5%",
    alignSelf: "center"
  },
  ProgressBar: {
    position: "absolute",
    height: "100%",
    backgroundColor: "#f27e24",
    borderRadius: 5
  },
  splitter: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    width: wp("60%"),
    alignSelf: "flex-end",
    marginRight: wp("5%")
  }
});

export default CourceList;
