import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import React from "react";
import { ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Bottom from "../../components/Bottom";
import CourceList from "./components/CourceList";
import ListItems from "../../components/ListItems";
import Search from "../../components/Search";
import CourcesData from "./components/CourcesData";
import CourceName from "./components/CourceName";
import CourseThumbnail from "./components/CourseThumbnail";

const CoursesHome = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>Cources</Text>
      </View>
      <Search />
      <ScrollView>
        <View style={styles.line}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.icon}>
              <MaterialIcons name="filter-list" size={rf(18)} color="#A1A2A9" />
            </View>
            <ListItems txt="All Cources" badge={328} />
            <ListItems txt="Sort By" />
            <ListItems txt="All Categories" />
          </ScrollView>
        </View>
        {/* {CourcesData['Designing'].items.map(i=>console.log(i.name))} */}
        {/* {CourcesData['Designing'].items.map(i=>console.log(i.name))} */}
        {CourceName.map((i) => (
          <View key={i.id}>
            <View style={styles.txtCont}>
              <Text style={styles.heading2}>{i.name}</Text>
              <TouchableOpacity
                onPress={() => navigation.navigate("Cources", { name: i.name })}
              >
                <Text style={{ fontSize: rf(13), color: "#f27e24" }}>
                  See all
                </Text>
              </TouchableOpacity>
            </View>
            <ScrollView
              horizontal={true}
              style={styles.thumbnail}
              showsHorizontalScrollIndicator={false}
            >
              {CourcesData[i.name].items.slice(0, 3).map((item) => (
                <CourseThumbnail key={item.id} {...item} />
              ))}
            </ScrollView>
          </View>
        ))}
      </ScrollView>
      <Bottom
        active="cources"
        onMembersPress={() => navigation.navigate("Members")}
        onGroupsPress={() => navigation.navigate("Groups")}
        onMorePress={() => navigation.navigate("More")}
        onHomePress={() => navigation.navigate("Home")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
    backgroundColor: "#fff"
  },
  txtCont: {
    paddingHorizontal: wp("4%"),
    marginTop: hp("3%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end"
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
  heading2: {
    color: "#000",
    fontSize: rf(20),
    fontWeight: "bold"
  },
  line: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    borderTopColor: "#EBEBEB",
    borderTopWidth: 1,
    height: hp("7%"),
    alignItems: "center",
    flexDirection: "row",
    paddingLeft: "5%",
    width: wp("100%")
  },
  icon: {
    width: hp("4"),
    height: hp("4%"),
    borderWidth: 1,
    borderColor: "#EBEBEB",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginRight: wp("2%")
  },
  cource: {
    marginTop: hp("1%")
  },
  thumbnail: {
    flexDirection: "row",
    width: wp("100%"),
    marginTop: hp("2%")
  }
});

export default CoursesHome;
