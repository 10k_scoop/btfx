import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Ionicons, Feather } from "@expo/vector-icons";

const LoginFields = ({ password, label }) => {
  const [pass, setPass] = useState(password);
  return (
      <View style={styles.cont2}>
        {label == "User Name" && (
          <Ionicons name="person-outline" size={rf(20)} color="#fff" />
        )}
        {label == "User Password" && (
          <Feather name="lock" size={rf(20)} color="#fff" />
        )}

        <TextInput
          placeholder={
            label == "User Password" ? "Enter Password" : "Enter " + label
          }
          style={styles.input}
          placeholderTextColor="#fff"
          secureTextEntry={pass}
        />
        {password && (
          <TouchableOpacity onPress={() => setPass(!pass)}>
            <Ionicons name="eye-outline" size={rf(20)} color="#fff" />
          </TouchableOpacity>
        )}
      </View>
  );
};

const styles = StyleSheet.create({
  cont2: {
    width: wp("84%"),
    height: hp("6%"),
    alignSelf: "center",
    backgroundColor: "#63A6DE",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "4%",
    borderRadius: 15,
    marginBottom:hp('2%')
  },
  input: {
    color: "#fff",
    width: "84%",
    height: "100%",
    alignSelf: "center",
    paddingLeft:5
  }
});

export default LoginFields;
