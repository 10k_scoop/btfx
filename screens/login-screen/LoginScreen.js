import React from "react";
import {
  Dimensions,
  ScrollView,
  StatusBar,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Feather } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import CustomButton from "../../components/CustomButton";
import LoginFields from "./components/LoginFields";

const LoginScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#0a4474", "#63A6DE", "#63A6DE"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <ScrollView>
        {/* Logo */}
        <View style={{ height: hp("40%") }}></View>
        {/* Text */}
        <View style={styles.logo}>
          <Image
            source={require("../../assets/logo1.png")}
            resizeMode="contain"
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: "#c4c3c3",
            }}
          />
        </View>
        <View style={{ alignItems: "center", height: hp("10%") }}>
          <Text style={styles.txt}>Sign in to BTFX Academy</Text>
        </View>
        {/* Login Fields */}
        <View style={{ height: hp("20%") }}>
          <LoginFields label="User Name" />
          <LoginFields label="User Password" password={true} />
          <TouchableOpacity
            style={styles.pass}
            onPress={() => navigation.navigate("ForgotPassword")}
          >
            <Text style={{ color: "#fff" }}>Forgot Password ?</Text>
          </TouchableOpacity>
        </View>
        {/* Login Button */}
        <View style={{ height: hp("10%"), justifyContent: "flex-end" }}>
          <CustomButton
            text="Login"
            screen="login"
            onPress={() => navigation.navigate("Home")}
          />
        </View>
        {/* Sign up */}
        <View style={[styles.cont, { height: hp("17%") }]}>
          <Text style={styles.signup}>Don't have an account?</Text>
          <TouchableOpacity
            style={styles.cont}
            onPress={() => navigation.navigate("SignUp")}
          >
            <Text style={[styles.signup, { fontWeight: "bold" }]}>
              {" "}
              Sign up{" "}
            </Text>
            <Feather name="arrow-right" size={rf(12)} color="#fff" />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  txt: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: rf(18),
  },
  pass: {
    alignSelf: "flex-end",
    marginRight: wp("10%"),
    top: -5,
  },
  signup: {
    color: "#fff",
    fontSize: rf(12),
  },
  cont: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "center",
  },
  logo: {
    width: wp("40%"),
    height: wp("40%"),
    alignItems: "center",
    position: "absolute",
    borderRadius: 100,
    borderWidth: 2,
    borderColor: "white",
    overflow: "hidden",
    top: hp("15%"),
    left: wp("30%"),
  },
});

export default LoginScreen;
