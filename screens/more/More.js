import React from 'react';
import { ScrollView, StatusBar, StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Bottom from '../../components/Bottom';
import Search from '../../components/Search';
import Label from './components/Label';
import List from './components/List';
import Profile from './components/Profile';

const More = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>More</Text>
      </View>
      <Search />
      <ScrollView>
        <Profile onPress={() => navigation.navigate("Profile")} />
        <Label txt="APP SETTINGS" />
        <View style={[styles.list, { height: hp("7.5%") }]}>
          <List txt="Settings" onPress={()=> navigation.navigate('Settings')} />
        </View>
        <Label txt="COMMUNITY" />
        <View style={[styles.list, { height: hp("45.5%") }]}>
          <List txt="Notifications" />
          <List txt="Messages" />
          <List txt="Forums" />
          <List txt="Discussions" />
          <List txt="Photos" />
          <List txt="Blog" />
        </View>
        <Label txt="LEARNDASH" />
        <View style={[styles.list, { height: hp("22.5%") }]}>
          <List txt="Courses" />
          <List txt="Course Categories" />
          <List txt="My Library" />
        </View>
        <Label txt="SAMPLE PAGES" />
        <View style={[styles.list, { height: hp("22.5%"), marginBottom:hp('3%')}]}>
          <List txt="Privacy Policy" />
          <List txt="Terms of Services" />
          <List txt="Web Pages" />
        </View>
      </ScrollView>
      <Bottom
        active="more"
        onMembersPress={() => navigation.navigate("Members")}
        onGroupsPress={() => navigation.navigate("Groups")}
        onHomePress={() => navigation.navigate("Home")}
        onCourcesPress={() => navigation.navigate("CoursesHome")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  txtCont: {
    paddingHorizontal: wp("4%"),
    backgroundColor: "#fff",
    paddingTop: StatusBar.currentHeight + hp('5%') 
  },
  heading: {
    color: "#000",
    fontSize: rf(25),
    fontWeight: "bold"
  },
  list:{
    backgroundColor:"#fff",
    alignSelf:"center",
    width:wp('90%'),
    borderRadius:15,
    overflow:"hidden",
    marginTop: hp('1%')
  }
});

export default More;
