import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Label = ({txt}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>{txt}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("6%"),
    justifyContent: "flex-end",
    paddingHorizontal: "7%"
  },
  txt: {
    fontSize: rf(13),
    color: "#979797",
    fontWeight: "bold"
  }
});

export default Label;
