import { Entypo } from "@expo/vector-icons";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Profile = ({onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.cont1}>
        <View style={styles.imgCont}>
          <Image
            source={require("../../../assets/display.jpg")}
            resizeMode="cover"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <View style={styles.txtCont}>
          <Text style={styles.name}>John</Text>
          <Text style={styles.duration}>@john</Text>
        </View>
        <Entypo name="chevron-right" size={rf(22)} color="#979797" />
      </View>
      <View style={styles.splitter}></View>
    </TouchableOpacity>
  );
};


const styles = StyleSheet.create({
  cont1: {
    width: wp("90%"),
    height: hp("12%"),
    flexDirection: "row",
    paddingHorizontal: wp("5%"),
    alignItems: "center",
    backgroundColor:"#fff",
    alignSelf:"center",
    borderRadius: 15,
    marginTop: hp('3%')
  },
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    overflow: "hidden"
  },
  txtCont: {
    width: "75%",
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: "5%",
  },
  name: {
    fontSize: rf(18),
    color: "#000",
    fontWeight: "bold"
  },
  duration: {
    fontSize: rf(12),
    color: "#979797"
  },
  splitter: {
    borderBottomColor: "#EBEBEB",
    borderBottomWidth: 1,
    width: wp("70%"),
    alignSelf: "flex-end",
    marginRight: wp("5%")
  }
});


export default Profile;
