import {
  AntDesign,
  Entypo,
  FontAwesome,
  FontAwesome5,
  Ionicons,
  MaterialIcons,
  SimpleLineIcons
} from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const List = ({ txt, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.iconCont}>
          <View
            style={[
              styles.icon,
              {
                backgroundColor:
                  txt == "Settings"
                    ? "#EB4E3D"
                    : txt == "Notifications" || txt == "My Library"
                    ? "#5756CE"
                    : txt == "Messages" || txt == "Privacy Policy"
                    ? "#3478F5"
                    : txt == "Forums" || txt == "Terms of Services"
                    ? "#09C89C"
                    : txt == "Discussions" ||
                      txt == "Courses" ||
                      txt == "Web Pages"
                    ? "#EB435A"
                    : txt == "Photos"
                    ? "#5756CF"
                    : txt == "Blog"
                    ? "#F29A37"
                    : "#65D160"
              }
            ]}
          >
            {txt == "Settings" && (
              <FontAwesome5 name="tools" size={rf(18)} color="#fff" />
            )}
            {txt == "Notifications" && (
              <Ionicons
                name="notifications-outline"
                size={rf(24)}
                color="#fff"
              />
            )}
            {txt == "Messages" && (
              <SimpleLineIcons name="envelope" size={rf(22)} color="#fff" />
            )}
            {txt == "Forums" && (
              <FontAwesome5 name="comments" size={rf(20)} color="#fff" />
            )}
            {txt == "Discussions" && (
              <MaterialIcons
                name="settings-display"
                size={rf(24)}
                color="#fff"
              />
            )}
            {txt == "Photos" && (
              <AntDesign name="picture" size={rf(24)} color="#fff" />
            )}
            {txt == "Blog" && (
              <FontAwesome name="edit" size={rf(24)} color="#fff" />
            )}
            {txt == "Courses" && (
              <Ionicons name="book-outline" size={rf(24)} color="#fff" />
            )}
            {txt == "Course Categories" && (
              <Ionicons name="ios-grid-outline" size={rf(24)} color="#fff" />
            )}
            {txt == "My Library" && (
              <Ionicons name="md-library-outline" size={rf(24)} color="#fff" />
            )}
            {txt == "Privacy Policy" && (
              <AntDesign name="filetext1" size={rf(20)} color="#fff" />
            )}
            {txt == "Terms of Services" && (
              <AntDesign name="filetext1" size={rf(20)} color="#fff" />
            )}
            {txt == "Web Pages" && (
              <Ionicons name="earth-outline" size={rf(24)} color="#fff" />
            )}
          </View>
          <Text style={styles.txt}>{txt}</Text>
        </View>
        <Entypo name="chevron-right" size={rf(22)} color="#979797" />
      </View>
      <View style={styles.splitter}></View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: hp("7.5%"),
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "5%"
  },
  iconCont: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "1%"
  },
  icon: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: 12,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp("3%"),
    overflow: "hidden"
  },
  txt: {
    fontWeight: "bold",
    fontSize: rf(16)
  },
  splitter: {
    borderBottomWidth: 1,
    borderBottomColor: "#c6c8cc",
    width: "90%",
    alignSelf: "flex-end"
  }
});

export default List;
