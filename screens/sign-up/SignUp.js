import React from "react";
import {
  Dimensions,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import TextField from "./components/TextField";
import { AntDesign } from "@expo/vector-icons";
import CustomButton from "../../components/CustomButton";

const SignUp = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#0a4474", "#63A6DE", "#63A6DE"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <AntDesign name="left" size={rf(18)} color="#fff" />
      </TouchableOpacity>
      <View style={styles.txtCont}>
        <Text style={styles.heading}>Create Account</Text>
      </View>
      <TextField label="Email" />
      <TextField label="Password" password={true} />
      <TextField label="First Name" />
      <TextField label="Last Name" />
      <TextField label="Nick Name" />
      <View style={{ marginTop: hp("2%") }}>
        <CustomButton text="Create Account" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  back: {
    marginLeft: wp("4%"),
    alignSelf: "flex-start",
  },
  heading: {
    color: "#fff",
    fontSize: rf(20),
    fontWeight: "bold",
  },
  txtCont: {
    marginLeft: wp("4%"),
    marginBottom: hp("5%"),
    marginTop: hp("3%"),
  },
});

export default SignUp;
