import React, { useState } from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Ionicons, Feather } from "@expo/vector-icons";

const TextField = ({password, label}) => {
    const [pass, setPass] = useState(password);
    return (
      <View style={styles.cont1}>
        {label != "User Name" &&
          label != "User Password" &&
          label != "Email Address" && <Text style={styles.label}>{label}</Text>}

        <View style={styles.cont2}>
          {label == "User Name" && (
            <Ionicons name="person-outline" size={rf(20)} color="#fff" />
          )}
          {label == "User Password" && (
            <Feather name="lock" size={rf(20)} color="#fff" />
          )}

          <TextInput
            placeholder={
              label == "User Password" ? "Enter Password" : "Enter " + label
            }
            style={styles.input}
            placeholderTextColor="#fff"
            secureTextEntry={pass}
          />
          {password && (
            <TouchableOpacity onPress={() => setPass(!pass)}>
              <Ionicons name="eye-outline" size={rf(20)} color="#fff" />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  cont1: {
    height: hp('13%'),
  },
  label: {
    color: "#fff",
    fontSize: rf(15),
    fontWeight: "bold",
    paddingLeft: wp("6%"),
    paddingBottom: 5
  },
  cont2: {
    width: wp("90%"),
    height: hp("6%"),
    alignSelf: "center",
    backgroundColor: "#63A6DE",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "4%",
    borderRadius: 15
  },
  input: {
    color: "#fff",
    width: "94%",
    height: "100%",
    alignSelf: "center",
  }
});

export default TextField;
